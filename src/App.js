import { useEffect , useState } from 'react';
import './App.css';
import Home from "./pages/Home";
import AppNavBar from './components/AppNavbar';
import Register from './pages/Register';
import Login from './pages/Login';
import Error from './pages/Error';
import Cart from './pages/Cart';
import Alert from './components/Alert';
import ProductPage from './pages/ProductPage';

import { UserProvider } from './UserContext';
import { Route, Switch, BrowserRouter as Router } from 'react-router-dom';
import Logout from './pages/Logout';
import Footer from './components/Footer';
import ProductView from './pages/Products';
import AdminDashboard from './pages/AdminDashboard';

function App() {

  const [ user, setUser ] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();

    setUser({
      id: null,
      isAdmin: null
  
    });
  }

  useEffect(() => {
    console.log(user);
      fetch('https://polar-refuge-67683.herokuapp.com/api/users/details', {
        headers: {
            Authorization: `Bearer ${ localStorage.getItem('access') }`
        }
      }).then(resultOfPromise => resultOfPromise.json())
      .then(convertedResult => {
        console.log(convertedResult);

        setUser({
            id: convertedResult._id,
            isAdmin: convertedResult.isAdmin
        }); 
        
        if (convertedResult._id !== "undefined") {
          setUser({
            id: convertedResult._id,
            isAdmin: convertedResult.isAdmin
          })
        } else {
          setUser({
            id: null,
            isAdmin: null
          })
        }
    })
  }, [])

  return (
    <UserProvider value={{user, unsetUser, setUser}}>
      <Router>
        <Alert />
        <AppNavBar />
        <Switch>
          <Route exact path ='/' component={Home} />
          <Route exact path ='/products' component={ProductPage} />
          <Route exact path ='/register' component={Register} />
          <Route exact path ='/logout' component={Logout} />
          <Route exact path ='/login' component={Login} />
          <Route exact path ="/products/:productId" component={ProductView} />
          <Route exact path = '/admin' component={AdminDashboard} />
          <Route exact path="/cart" component={Cart} />
          <Route component={Error} />
        </Switch>
        <Footer />
      </Router>
    </UserProvider>
  )
}

export default App;
