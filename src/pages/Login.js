import UserContext from '../UserContext';
import React, { useState, useEffect, useContext } from 'react';
import {Form, Button, Container } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { Redirect, NavLink, Link } from 'react-router-dom';

//create a function that will describe the anatomy of the page.



export default function Login() {

    const {user, setUser} = useContext(UserContext);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    const authenticate = (e) => {
        e.preventDefault()

        fetch('https://polar-refuge-67683.herokuapp.com/api/users/login', {
            method: "POST",
            headers: {
                'Content-type' : 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            // console.log(data);
            // localStorage - stores information to our web pages (keeps the data forever)
            // setItem - sets the data to our localStorage
            // getItem - get the data from our localStorage

            if(typeof data.access !== "undefined") {
                localStorage.setItem('access', data.access);
                retrieveUserDetails(data.access);
                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to the App!"
                })
            } else {
                Swal.fire({
                    title: "Authentication Failed",
                    icon: "error",
                    text: "Check if your credentials are valid!"
                })
            }

        })
    }

    const retrieveUserDetails = (token) => {
        //will allow us to send a request that will allow us to retrieve the details about the user from the mongoDB database

        fetch('https://polar-refuge-67683.herokuapp.com/api/users/details', {
            headers: {
                Authorization: `Bearer ${token}`
            }
        }).then(resultOfPromise => resultOfPromise.json())
        .then(convertedResult => {
            console.log(convertedResult);

            setUser({
                id: convertedResult._id,
                isAdmin: convertedResult.isAdmin
            })
        })
    }

    useEffect(() => {
        if(email !== "" && password !== ""){
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [email, password]);

    return(

        (user.isAdmin === true) ?
        <Redirect to="/admin" />
        :

        (user.id) ? 
        
        <Redirect to="/products" />
        
        : 
        <React.Fragment>
            <Container className="Margin my-5">
                <h1>Login to Your Account</h1>
                {/* Login Form Component */}
                <Form className="mb-5" onSubmit={ e => authenticate(e)}>

                     {/* For User's Email */}
                    <Form.Group controlId="userEmail">
                        <Form.Label className="mt-3">Email Address: </Form.Label>
                        <Form.Control type="email" className="formBar" placeholder="Insert Email Here"
                        value = { email }
                        onChange={ event => setEmail(event.target.value) }
                        required/>
                    </Form.Group>

                     {/* For User's Password */}
                    <Form.Group controlId="userPassword">
                        <Form.Label className="mt-3">Password: </Form.Label>
                        <Form.Control type="password" className="formBar" placeholder="Insert Password Here" 
                        onChange = { event => setPassword(event.target.value)}
                        value = { password }
                        required/>
                    </Form.Group>

                    {/* Button Component */}

                    {
                        isActive ?
                        
                        <Button type="submit" id="submitBtn" className="btn btn-block mt-3 btnBar">
                            LOGIN
                        </Button>
                        :
                        <Button type="submit" id="submitBtn"        variant="secondary" className="btn btn-block mt-3 btnBarGray" disabled>
                            LOGIN
                        </Button>
                    }

                    <Link className="mt-5 registerLink" as={NavLink} to="/register">No account yet? Sign up Here</Link>

                </Form>
            </Container>
        </React.Fragment>
    );
}
