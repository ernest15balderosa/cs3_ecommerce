import { Container, Image } from 'react-bootstrap';


export default function Placeholder() {
    return(
            <Container className="my-5 text-center">
                <h1>Sorry, Are you lost? This Page Doesn't Exist!</h1>
                <Image className="mt-5" src="https://i.pinimg.com/originals/a8/12/1a/a8121abee959e18cbad25ad4046f76d8.gif" />
            </Container>
    );
}
