import React, { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import ProductCard from '../components/ProductCard';
import BannerProduct from '../components/BannerProduct';

export default function Products() {

    const [ products, setProduct ] = useState([]);

    useEffect(() => {
        fetch('https://polar-refuge-67683.herokuapp.com/api/products').then( result => result.json()).then( convertedData => {
            console.log(convertedData);

            setProduct(convertedData.map(product => {
                return(
                    <ProductCard key={product._id} data={product}/>
                );
            }));
        });
    })

    return(
        <React.Fragment>
            <BannerProduct />
            <Container>
                { products }
            </Container>
        </React.Fragment>
    );
}