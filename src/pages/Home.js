import React from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

const Home = () => {
    return (
        <div>
            <Banner />
            <Highlights />
        </div>
    )
}

export default Home;