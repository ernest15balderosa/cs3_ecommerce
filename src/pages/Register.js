import React, { useState, useEffect } from 'react';
import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { Redirect, useHistory } from 'react-router-dom';

const Register = () => {
    
    const history = useHistory();
    const [ firstName, setFirstName ] = useState('');
    const [ middleName, setMiddleName ] = useState('');
    const [ lastName, setLastName ] = useState('');
    const [ email, setEmail ] = useState('');
    const [ password1, setPassword1 ] = useState('');
    const [ password2, setPassword2 ] = useState('');
    const [ mobileNo, setMobileNo ] = useState('');

    const [ isRegisterBtnActive, setRegisterBtnActive ] = useState('');
    const [ isComplete, setIsComplete ] = useState(false);
    const [ isMatched, setIsMatched ] = useState(false);
    const [ isValidMobile, setIsValidMobile ] = useState(false);

    const slicedMobile = mobileNo.slice(0,4);

    function registerUser(event) {
        event.preventDefault();
        
        fetch(`https://polar-refuge-67683.herokuapp.com/api/users/register`, {
            method: "POST",
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify({
                firstName: firstName,
                middleName: middleName,
                lastName: lastName,
                email: email,
                password: password1,
                mobileNo: mobileNo
            })
        })
        .then(res => res.json())
        .then(data => {
            Swal.fire({
                title: `Your account is registered succesfully!`,
                icon: 'success',
                text: 'You can now shop with us!'
            })

            history.push('/login');
        });

    }
        
        
        useEffect( () => {

            if ( slicedMobile === '0995' || slicedMobile === '0917' || slicedMobile === '0998' || slicedMobile === '0908' || slicedMobile === '0919' || slicedMobile === '0975') {
                setIsValidMobile(true);
            } else if(firstName !== '' && middleName !== '' && lastName !== '' && email !== '' && password1 !== '' && password2 !== '' && mobileNo !== ''){
                setRegisterBtnActive(true);
                setIsComplete(true);
            } else if ((password1 === password2) && password1 !== '') {
                setIsMatched(true);
            } else {
                setRegisterBtnActive(false);
                setIsComplete(false);
                setIsMatched(false);
                setIsValidMobile(false);
            }
        }, [firstName, middleName, lastName, email, password1, password2, mobileNo, slicedMobile])
        

    return(
        <React.Fragment>
            <Container className="mt-5">

                {
                    isComplete ?
                    <h1 className="mb-5">Almost there! Click the Register Button</h1>
                    :
                    <h1 className="mb-5">We'll Turn Your House into a Home!</h1>
                }

                {/* Register Form */}
                <Form onSubmit={(e) => registerUser(e)} >

                    {/* first name */}
                    <Form.Group controlId="firstName">
                        <Form.Label>First Name </Form.Label>
                        <Form.Control 
                        type="text" 
                        className="formBar"
                        placeholder="Insert your First Name here" 
                        value={firstName} 
                        onChange={ event => setFirstName(event.target.value) }
                        required    
                        />
                    </Form.Group>

                    {/* Middle Name */}
                    <Form.Group controlId="middleName" className="mt-4">
                        <Form.Label>Middle Name </Form.Label>
                        <Form.Control 
                        type="text" 
                        className="formBar"
                        placeholder="Insert your Middle Name here" 
                        value={middleName} 
                        onChange={ event => setMiddleName(event.target.value) }
                        required
                        />
                    </Form.Group>

                    {/* Last Name */}
                    <Form.Group controlId="lastName" className="mt-4">
                        <Form.Label>Last Name </Form.Label>
                        <Form.Control 
                        type="text" 
                        className="formBar"
                        placeholder="Insert your Last Name here" 
                        value={lastName} 
                        onChange={ event => setLastName(event.target.value) }
                        required
                        />
                    </Form.Group>

                    {/* Email */}
                    <Form.Group controlId="email" className="mt-4">
                        <Form.Label>Email </Form.Label>
                        <Form.Control 
                        type="email" 
                        className="formBar"
                        placeholder="Insert your Email here" 
                        value={email} 
                        onChange={ event => setEmail(event.target.value) }
                        required
                        />
                    </Form.Group>

                    {/* Password */}
                    <Form.Group controlId="password1" className="mt-4">
                        <Form.Label>Password </Form.Label>
                        <Form.Control 
                        type="password" 
                        className="formBar"
                        placeholder="Insert your Password here" 
                        value={password1} 
                        onChange={ event => setPassword1(event.target.value) }
                        required
                        />
                    </Form.Group>
                    
                    
                    {/* Confirm Password */}
                    
                    {
                        isMatched ?

                        <Form.Group controlId="password2" className="mt-4">
                            <Form.Label>Confirm Password </Form.Label>
                                <Form.Control 
                                    type="password"
                                    className="formBar"
                                    placeholder="Confirm Password Here" 
                                    value={password2} 
                                    onChange={ event => setPassword2(event.target.value) }
                                    required
                                />
                        </Form.Group>
                        :
                        <Form.Group controlId="password2" className="mt-4">
                            <Form.Label>Confirm Password </Form.Label>
                                <Form.Control 
                                    type="password" 
                                    className="formBarRed"
                                    placeholder="Confirm Password Here" 
                                    value={password2} 
                                    onChange={ event => setPassword2(event.target.value) }
                                    required
                                />
                        </Form.Group>

                    }


                    {/* mobile number */}
                    {
                        isValidMobile ?
                        <Form.Group controlId="mobileNumber" className="mt-4">
                            <Form.Label>Mobile Number </Form.Label>
                                <Form.Control 
                                type="number" 
                                className="formBar"
                                placeholder="Insert Mobile Number Here" 
                                value={mobileNo}
                                onChange = { event => setMobileNo(event.target.value) }
                                required
                                onInput = {(e) =>{e.target.value = e.target.value.slice(0,11)}}
                                />
                        </Form.Group>
                        :
                        <Form.Group controlId="mobileNumber" className="mt-4">
                            <Form.Label>Mobile Number </Form.Label>
                                <Form.Control 
                                type="number" 
                                className="formBarRed"
                                placeholder="Insert Mobile Number Here" 
                                value={mobileNo}
                                onChange = { event => setMobileNo(event.target.value) }
                                required
                                onInput = {(e) =>{e.target.value = e.target.value.slice(0,11)}}
                                />
                        </Form.Group>
                    }
                    

                    { isRegisterBtnActive ?
                    <Button
                    type="submit" 
                    variant="warning"
                    className="btn btn-block btnBar mt-4 mb-5">
                        Register
                    </Button>
                    :
                    <Button 
                    type="submit" 
                    variant="secondary"
                    className="btn btn-block btnBarGray mt-4 mb-5" 
                    disabled>
                        Incomplete Details
                    </Button>
                    }

                </Form>

            </Container>
        </React.Fragment>
    );
}

export default Register;