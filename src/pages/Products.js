import { useContext, useState, useEffect } from 'react';
import { Link, NavLink, useParams } from 'react-router-dom';
import UserContext from '../UserContext';
import { Container, Row, Col, Card, Button } from 'react-bootstrap';
import styled from 'styled-components';

const Wrapper = styled.div`

	@media (max-width: 480px) {
		flex: 1;
		height: 250px;
		width: 250px;
		align-content: center;
  	}

	height: 800px;
	width: 800px;

`

const Image = styled.img`
	height: 100%;
	margin-left: auto;
	margin-right: auto;
`;

export default function ProductView({data}) {

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [image, setImage] = useState("");
	const [price, setPrice] = useState(0);


	const { productId } = useParams();
	const { user } = useContext(UserContext);

	useEffect(()=> {

		console.log(productId);
	
		fetch(`https://polar-refuge-67683.herokuapp.com/api/products/${productId}`)
		.then(res => res.json())
		.then(data => {
	
			console.log(data);
	
			setName(data.name);
			setImage(data.image);
			setDescription(data.description);
			setPrice(data.price);
	
		});
	
	}, [productId]);


    return (
		<Container fluid>
			<Row>
				<Col>
					<Wrapper>
						<Image className="m-5" src={image} /> 
					</Wrapper>
				</Col>
				<Col className="m-5">
					<h1 className="bold">{name}</h1>
					<p className="mt-4">{description}</p>
					<Row className="mt-5">
						<Col>
							<p className="bold">Php {price}</p>
						</Col>
						<Col className="justify flex-end">
						{ (user.id) ? 
							<Button className="btn btn-block btnBarProd" as={NavLink} to="/cart">Add to Cart</Button>
						: 
            				<Link className="btn btn-block btnBarProd" to="/login">Log in to Buy</Link>
						}
						</Col>
					</Row>
				</Col>
			</Row>
		</Container>

    );
 }
