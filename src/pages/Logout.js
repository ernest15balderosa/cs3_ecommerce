import React, { useContext, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout() {
    
    const { unsetUser, setUser, user } = useContext(UserContext)
    
    unsetUser();

    useEffect(() => {
        setUser({id: null});
    })

    return(
        (user.id) ?
            <h1>Logging Out</h1>
        :
        <Redirect to="/login"></Redirect>
    );
}