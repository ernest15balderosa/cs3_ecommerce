import { Badge } from '@material-ui/core';
import { ShoppingCartOutlined } from '@material-ui/icons';
import React, { useContext } from 'react'
import { Nav } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import styled from 'styled-components';
import UserContext from '../UserContext';
import { mobile } from "../Responsive";

const Container = styled.div`
    height: 70px;
    background-color: #FFFFFF;
    ${mobile({ height: "50px" })}
`;

const Wrapper = styled.div`
    padding: 10px 20px;
    display: flex;
    justify-content: space-between;
    ${mobile({ padding: "10px 0px" })}
`;

const Left = styled.div`
    flex:1;
    display: flex;
    align-items: center;
`;

const Right = styled.div`
    flex:1;
    align-items: center;
    display: flex;
    align-items: center;
    justify-content: flex-end;
    ${mobile({ flex: 2, justifyContent: "center" })}
`;

const Center = styled.div`
    flex:1;
    align-items: center;
`;

const Logo = styled.h1`
    font-weight: bold;
    text-align: center;
    color: #330066;
    ${mobile({ fontSize: "24px" })}
`

const MenuItem = styled.div`
    font-size: 14px;
    cursor: pointer;
    margin-left: 25px;
    color: #330066;
    ${mobile({ fontSize: "12px", marginLeft: "10px" })}
`
const NavBar = () => {
    
    const { user } = useContext(UserContext);

    return (
        <Container>
            <Wrapper>
                {
                    (user.isAdmin === true )?
                        <Right>
                            <MenuItem>
                                <Nav.Link className="navlinks" as={NavLink} to="/logout">
                                        LOGOUT
                                </Nav.Link>
                            </MenuItem>
                        </Right>
                        :
                        <>
                        <Left>
                    <MenuItem>
                        <Nav.Link className="navlinks" as={NavLink} to="/">
                            HOME
                        </Nav.Link>
                    </MenuItem>
                    <MenuItem>
                        <Nav.Link className="navlinks" as={NavLink} to="/products">
                            PRODUCTS
                        </Nav.Link>
                    </MenuItem>
                </Left>
                <Center className="hidden-xs">
                    <Logo>UKEA.</Logo>
                </Center>
                    <Right>
                    {
                        (user.id) ?
                        <>
                            <MenuItem>
                                <Nav.Link className="navlinks" as={NavLink} to="/logout">
                                    LOGOUT
                                </Nav.Link>
                            </MenuItem>
                            <MenuItem>
                            <Badge badgeContent={4} color="primary">
                                <Nav.Link as={NavLink} to="/cart"> 
                                    <ShoppingCartOutlined />
                                </Nav.Link>
                            </Badge>
                            </MenuItem>
                        </>
                        :
                        <>
                            <MenuItem className="hidden-xs">
                                <Nav.Link className="navlinks" as={NavLink} to="/register">
                                    REGISTER
                                </Nav.Link>
                            </MenuItem>
                            <MenuItem>
                                <Nav.Link className="navlinks" as={NavLink} to="/login">
                                    LOGIN
                                </Nav.Link>
                            </MenuItem>
                        </>
                    }
                    </Right>
                        </>
                }
            </Wrapper>
        </Container>
    )
}

export default NavBar