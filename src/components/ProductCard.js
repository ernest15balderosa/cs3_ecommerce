import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { SearchOutlined } from "@material-ui/icons";  

const Info = styled.div`
	opacity: 0;
	width: 100%;
	height: 100%;
	position: absolute;
	top: 0;
	left: 0;
	background-color: rgba(0, 0, 0, 0.2);
	z-index: 3;
	display: flex;
	align-items: center;
	justify-content: center;
	transition: all 0.5s ease;
	cursor: pointer;
`;
  
const Container = styled.div`
	flex: 1;
	margin: 5px;
	min-width: 280px;
	height: 350px;
	display: flex;
	align-items: center;
	justify-content: center;
	position: relative;
	&:hover ${Info}{
	  opacity: 1;
	}
`;
  
  
const Image = styled.img`
	height: 100%;
	@media (max-width: 767px){
		height: 70%
	} 
`;
  
const Icon = styled.div`
	width: 40px;
	height: 40px;
	border-radius: 50%;
	background-color: white;
	display: flex;
	align-items: center;
	justify-content: center;
	margin: 10px;
	transition: all 0.5s ease;
	&:hover {
	  background-color: #e9f5f5;
	  transform: scale(1.1);
	}
`;


export default function ProductCard({data}) {

	const { _id, name, description, image, price } = data;

	return(
		<>
		<hr class="solid" />
		<h2 className="mt-3 bold">{name}</h2>
		<p>{description}</p>
		<Container>
		<Image src={image} />
		<Info>
		  <Icon>
			<Link to={`/products/${_id}`}>
			<SearchOutlined />
			</Link>
		  </Icon>
		  <h5 className='whiteText'>Starts at Php {price}</h5>
		</Info>
	  	</Container>
		</>
	);
}
  