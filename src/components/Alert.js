import styled from "styled-components";

const Container = styled.div`
    height: 30px;
    background-color: #330066;
    color: white;
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 14px;
    font-weight: bold;
`

export default function Alert() {
    return (
        <Container>
            FREE SHIPPING NATIONWIDE UNTIL OCT.31!
        </Container>
    )
}
