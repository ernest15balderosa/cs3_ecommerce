import React, { useState } from 'react';
import styled from 'styled-components';
import { bannerPhotos } from './info';
import { NavLink } from 'react-router-dom';

const Container = styled.div`
    width: 100%;
    height: 95vh;
    display: flex;
    position: relative;
    overflow: hidden;
    @media (max-width: 767px) {
    height: 50vh;
    }
`;

const Wrapper = styled.div`
    height: 100%;
    display: flex;
`;

const Slide = styled.div`
    width: 100vw;
    height: 100vh;
    display: flex;
    alignt-items: center;
    background-color: #${props => props.bg}
`;

const ImageContainer = styled.div`
    height: 100%;
    flex: 1;
`;

const Image = styled.img`
    height: 100%;
`;

const InfoContainer = styled.div`
    flex: 1;
    padding: 50px;
    vertical-align: 50vh;
`;

const Title = styled.h1`
    font-size: 70px;
    color: #330066;
    @media (max-width: 767px) {
    font-size: 25px;
    }
`;

const Description = styled.p`
    margin: 50px 0px;
    font-size: 20px;
    font-weight: 500;
    letter-spacing: 3px;
    @media (max-width: 767px) {
    font-size: 10px;
    }
`;

const Button = styled.button`
    pading: 30px;
    font-size: 20px;
    background-color: #330066;
    cursor: pointer;
    color: white;
    border-radius: 5px;
    border-color: transparent;
    @media (max-width: 767px) {
        font-size: 15px;
        padding: 5px;
    }
`;

const Banner = () => {

    return (
        <Container>
            <Wrapper>
            {bannerPhotos.map((photos) => (
                <Slide bg={photos.bg}>
                <InfoContainer>
                        <Title>{photos.title}</Title>
                        <Description>{photos.desc}</Description>
                        <Button as={NavLink} to='/products'>Shop Now!</Button>
                    </InfoContainer>
                    <ImageContainer>
                        <Image src={photos.img} />
                    </ImageContainer>
                </Slide>
                ))}
            </Wrapper>
        </Container>
    )
}

export default Banner;
