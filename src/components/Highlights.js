import React from 'react'
import { Row, Col, Card, Container, CardImg } from 'react-bootstrap';
import  '../App.css';
import styled from 'styled-components';

const Highlights = () => {
    return(
        <Container fluid id="ReviewTab">
            <Row>
                <Col>
                    <Card className="Review">
                        <Card.Title id="ReviewTitle">
                            CUSTOMER REVIEWS
                        </Card.Title>
                        <Card.Subtitle id="ReviewText">
                            Let the feedback talk for us. You're on good hands.
                        </Card.Subtitle>
                    </Card>
                </Col>
            </Row>
            <Row className="mt-5 text-center">
                <Col>
                    <Card className="Review">
                        <CardImg className="ReviewPhoto" src="https://images.pexels.com/photos/2762247/pexels-photo-2762247.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"/>
                        <Card.Title>
                            Classy but Affordable!
                        </Card.Title>
                        <Card.Subtitle>
                            John H.
                        </Card.Subtitle>
                    </Card>
                </Col>
                <Col>
                    <Card className="Review">
                        <CardImg className="ReviewPhoto" src="https://images.pexels.com/photos/4765366/pexels-photo-4765366.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"/>
                        <Card.Title>
                            Bang for the buck!
                        </Card.Title>
                        <Card.Subtitle>
                            Maria B.
                        </Card.Subtitle>
                    </Card>
                </Col>
                <Col>
                    <Card className="Review" id="lastCardHighlight">
                        <CardImg className="ReviewPhoto" src="https://images.pexels.com/photos/1067556/pexels-photo-1067556.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"/>
                        <Card.Title>
                            Wide Selection!
                        </Card.Title>
                        <Card.Subtitle>
                            Keifer L.
                        </Card.Subtitle>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}

export default Highlights;