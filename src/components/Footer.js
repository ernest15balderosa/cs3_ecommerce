import { Container, Col, Row } from 'react-bootstrap';

export default function Footer() {
    return(
            <Container fluid className="footer"> 
                <Row className="mt-5">
                    <Col className="m-5">
                        <h3 className="footer-content mb-5">About Us</h3>
                        <p className="footer-content">We are the Philippines' largest furniture manufacturer since 1930. We are located in the heart of Manila. For custom orders please message us at ukea.ph@gmail.com</p>
                    </Col>
                    <Col className="footer-content-name hidden-xs">
                        <h3 className="footer-content mb-5">
                            Contact
                        </h3>
                        <p className="footer-content">
                            Ernest Y. Balderosa | ernestbalderosa@gmail.com
                        </p>
                        <p className="footer-content">
                            Want a website like this? Contact me with my information above.
                        </p>
                    </Col>
                </Row>
            </Container>
        
            
        
    );
}