import '../Admin.css';
import Swal from 'sweetalert2';
import { useState } from 'react';

export default function NewProduct() {

    const [ name, setName ] = useState('');
    const [ description, setDescription ] = useState('');
    const [ image, setImage ] = useState('');
    const [ price, setPrice ] = useState('');

    function addProduct(event){
        event.preventDefault();

        fetch(`https://polar-refuge-67683.herokuapp.com/api/products/addProduct`, {
            method: "POST",
            headers: {
                'Content-type': 'application/json',
                Authorization: `Bearer ${ localStorage.getItem('access') }`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                image: image,
                price: price
            })
        })
        .then(res => res.json())
        .then(data => {
            Swal.fire({
                title: `Product Successfully Created!`,
                icon: 'success',
                text: 'You can now check it out on the product page.'
            })

        })
    }

    return (
        <div className="newProduct mx-5">
            <h1 className="addProductTitle">New Product</h1>
            <form className="addProductForm" onSubmit={(e) => addProduct(e)}>
                
                <div className="addProductItem">
                <label>Name</label>
                <input type="text" 
                placeholder="Add Product Name" 
                value={name} 
                onChange={event => setName(event.target.value)}
                required
                />
                </div>

                <div className="addProductItem">
                <label>Description</label>
                <input type="text" 
                placeholder="Add Product Description" 
                value={description} 
                onChange={event => setDescription(event.target.value)}
                required    
                />
                </div>


                <div className="addProductItem">
                <label>Image</label>
                <input type="text" 
                placeholder="Add Image Link" 
                value={image} 
                onChange={event => setImage(event.target.value)}
                required
                />
                </div>

                <div className="addProductItem">
                <label>Price</label>
                <input type="number" 
                placeholder="Add Product Price" 
                value={price} 
                onChange={event => setPrice(event.target.value)}
                required/>
                </div>

                <button type="submit" className="addProductButton">Create</button>
            </form>
        </div>
    )
}
