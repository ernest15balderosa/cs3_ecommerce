import React, { useContext } from 'react';
import '../App.css';
import UserContext from '../UserContext';

const BannerProduct = () => {

    const { user } = useContext(UserContext);

    return (

            <header>
                <div className = "head-text">
                    <div className = "head-image">
                    <img src = "https://gcdn.pbrd.co/images/udpDVsKqkh7R.png?o=1"/>
                    </div>
                    <div class='text-on-image'>
                        <h1 className='whiteText bold'>We Will Turn Your House Into a Home! </h1>

                        { (user.id) ?
                            <>
                            <p className='whiteText'>Perks and discounts await you! Make sure to check our product page frequently! </p>
                            </>
                            :
                            <p className='whiteText'>Make sure to be logged in before shopping! </p>
                        }

                    </div>
                </div>
            </header>
    )
}

export default BannerProduct;
