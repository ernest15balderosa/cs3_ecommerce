export const bannerPhotos = [
    {
        img: "https://www.mydomaine.com/thmb/kY5OfTSYNMMfvhE2uqdix01V2Zc=/1000x667/filters:fill(auto,1)/Design_CathieHongInteriorsPhoto_ChristyQPhotography-1f0a7254eb7a435aa80046b0bf60d33b.jpg",
        title: "NEW ARRIVAL",
        desc: "Check out these trendy mirrors that will surely make your home standout!",
        bg: "F5F5F5",
    }
];
