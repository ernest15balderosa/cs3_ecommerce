import { Container, Image } from 'react-bootstrap';


export default function Placeholder() {
    return(
            <Container className="my-5 text-center">
                <h1>Oops! This page is stil under construction. We're working day and night to get this ready for you!</h1>
                <Image className="mt-5" src="https://i.pinimg.com/originals/66/83/3e/66833e07d6fb9eb5d724e47d0c814285.gif" />
            </Container>
    );
}
